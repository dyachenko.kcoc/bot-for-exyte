# Bot for Exyte

Implement a small exchange rate telegram bot.

To get started, copy config.py.example and rename it to config.py then insert token. 

Also you need to install pipenv via pip.

Use pipenv to create a virtual environment, activate it,
and install the necessary packages (pipenv install).

