import os
import sqlite3
from datetime import datetime, timedelta, date
import matplotlib
import matplotlib.pyplot as plt
import requests
import telebot

import config

types = telebot.types  # for markup bottom

# create a bot
bot = telebot.TeleBot(config.TOKEN_TELEGRAM_BOT)  # create a new Telegram Bot object

# create db if is not exists
connect = sqlite3.connect('currency_exchange.db', check_same_thread=False)
cursor = connect.cursor()

# create tables in db
cursor.execute('''CREATE TABLE IF NOT EXISTS currency_quotes
                (quote_number, currency_in_quote, rates_in_quote)''')  # date types: null, integer, real, text и blob
connect.commit()


def make_request_and_save_response_to_db():
    # make request
    params = {'access_key': config.TOKEN_CURRENCY}
    r = requests.get('http://data.fixer.io/api/latest', params=params)
    response = r.json()

    quotes = response.get('rates')

    # from quotes to list
    currency_list = list(quotes.keys())
    rates_list = list(quotes.values())

    # multiplier to change from EUR to USD main currency
    multiplier = 1 / quotes['USD']

    # make rates_list from EUR to USD main currency through multiplier
    rates_list = [c * multiplier for c in rates_list]

    # delete previous requests data in db
    cursor.execute('DELETE FROM currency_quotes')
    connect.commit()

    # save time response to response_dict
    response_dict['request_time'] = datetime.now().replace(microsecond=0)

    # insert new values quotes into db
    for quote in range(len(quotes)):
        present_quote_number = quote
        present_currency_in_quote = currency_list[quote]
        present_rates_in_quote = rates_list[quote]
        cursor.execute(f"""INSERT INTO currency_quotes
                       VALUES ({present_quote_number},
                            '{present_currency_in_quote}',
                            {present_rates_in_quote})
                       """)
    connect.commit()


def get_response_data_from_db():
    # Get quote_number from db
    cursor.execute(f"""SELECT quote_number FROM currency_quotes""")
    quote_number_from_db = cursor.fetchall()

    # Get rates_in_quote from db
    cursor.execute(f"""SELECT rates_in_quote FROM currency_quotes""")
    rates_in_quote_from_db = cursor.fetchall()

    # Get currency_in_quote from db
    cursor.execute(f"""SELECT currency_in_quote FROM currency_quotes""")
    currency_in_quote = cursor.fetchall()

    #  make list from tuples
    numbers_of_quotes = []
    list_of_currency = []
    list_of_rates = []
    for m in range(0, len(quote_number_from_db)):
        numbers_of_quotes.append(quote_number_from_db[m][0])
        list_of_currency.append(currency_in_quote[m][0])
        list_of_rates.append(rates_in_quote_from_db[m][0])

    # save response data from db to response_dict
    response_dict['quote_numbers'] = numbers_of_quotes
    response_dict['currency_list'] = list_of_currency
    response_dict['rates_list'] = list_of_rates


# Check if pass 10 minutes from last request to make new
def make_new_request_if_10minutes_passed():
    now_time_minus_time_request = datetime.now().replace(microsecond=0) - response_dict['request_time']
    # print(response_dict['request_time'], 'request_time')
    # print(datetime.now().replace(microsecond=0), 'datetime.now')
    # print(now_time_minus_time_request, 'datetime.now minus request_time')

    if now_time_minus_time_request >= timedelta(minutes=10):
        print('passed more then 10 min, make new request')
        make_request_and_save_response_to_db()

    else:
        print('passed less then 10 min,- used previous data')


# start
@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id,
                     f'Welcome, {message.from_user.first_name}!\nChoose <b>{bot.get_me().first_name}</b>option:\n'
                     ' /list - all currency list rates to USD\n'
                     ' /convert - exchange rates any currency\n'
                     ' /history - currency rates change history for last 7 days', parse_mode='html')


# send list of quotes
@bot.message_handler(commands=['list'])
def quotes_list(message):
    # make_new_request_if_10minutes_passed()
    get_response_data_from_db()
    currency_list = response_dict['currency_list']
    rates_list = response_dict['rates_list']
    quote_list = response_dict['quote_numbers']
    string = ''
    for m in range(len(quote_list)):
        curr = currency_list[m]
        rat = str(round(rates_list[m], 2))
        string += curr + ': ' + rat + '\n'
        if len(string) >= 4000:
            bot.send_message(message.chat.id, string)
            string = ''
    bot.send_message(message.chat.id, string)
    bot.send_message(message.chat.id, '/start')


# convert
@bot.message_handler(commands=['convert'])
def from_currency_choice(message):
    make_new_request_if_10minutes_passed()
    get_response_data_from_db()
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    itembtn1 = types.KeyboardButton('EUR')
    itembtn2 = types.KeyboardButton('USD')
    itembtn3 = types.KeyboardButton('UAH')
    itembtn4 = types.KeyboardButton('AUD')
    itembtn5 = types.KeyboardButton('CAD')
    itembtn6 = types.KeyboardButton('RUB')
    markup.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5, itembtn6)
    msg = bot.send_message(message.chat.id, 'Що у вас є? Виберіть з запропонованих валют для обміну,'
                                            'або введіть в форматі: UAH, наприклад.'
                           , reply_markup=markup)
    bot.register_next_step_handler(msg, from_currency_save)


def from_currency_save(message):
    curr_from = message.text
    if curr_from not in response_dict['currency_list']:
        msg = bot.reply_to(message,
                           'Нема такої валюти, або записано неправильно, перевірте, будь ласка')
        bot.register_next_step_handler(msg, from_currency_save)
        return
    convert_dict['curr_from'] = curr_from
    msg = bot.reply_to(message,
                       f'Міняємо {convert_dict["curr_from"]}, введіть суму для обміну, скільки у вас є?',
                       parse_mode='html')
    bot.register_next_step_handler(msg, from_amount_of_money)


def from_amount_of_money(message):
    amount_from = message.text
    if not amount_from.isdigit():
        msg = bot.reply_to(message, 'Введіть суму правильно, числом')
        bot.register_next_step_handler(msg, from_amount_of_money)
        return

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)  # bottom on row width
    itembtn1 = types.KeyboardButton('EUR')
    itembtn2 = types.KeyboardButton('USD')
    itembtn3 = types.KeyboardButton('UAH')
    itembtn4 = types.KeyboardButton('AUD')
    itembtn5 = types.KeyboardButton('CAD')
    itembtn6 = types.KeyboardButton('RUB')
    markup.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5, itembtn6)

    convert_dict['amount_from'] = amount_from
    msg = bot.reply_to(message,
                       f'Міняємо  {message.text} {convert_dict["curr_from"]}.\n '
                       f'Що вам потрібно, на яку валюту міняємо? \n '
                       'Виберіть валюту iз запропонованих, або введіть в форматі: UAH, наприклад.'
                       , parse_mode='html', reply_markup=markup)
    bot.register_next_step_handler(msg, to_currency_save)


def to_currency_save(message):
    curr_to = message.text
    if curr_to not in response_dict['currency_list']:
        msg = bot.reply_to(message,
                           'Нема такої валюти, або записано неправильно, перевірте, будь ласка')
        bot.register_next_step_handler(msg, to_currency_save)
        return

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    itembtn1 = types.KeyboardButton('Так')
    itembtn2 = types.KeyboardButton('Ні')
    markup.add(itembtn1, itembtn2)

    convert_dict['curr_to'] = curr_to
    msg = bot.reply_to(message,
                       f'Міняємо {convert_dict["amount_from"]}'
                       f'{convert_dict["curr_from"]} на {convert_dict["curr_to"]}, все правильно?',
                       parse_mode='html', reply_markup=markup)
    bot.register_next_step_handler(msg, confirmation_and_calculation)


def confirmation_and_calculation(message):
    confirmation = message.text
    if confirmation not in ('Так', 'Ні'):
        msg = bot.reply_to(message,
                           'Виберіть з двох варіантів, будь ласка')
        bot.register_next_step_handler(msg, confirmation_and_calculation)
        return
    elif confirmation == 'Так':
        """Get rates from db"""
        cursor.execute(f"""SELECT rates_in_quote
                           FROM currency_quotes
                           WHERE currency_in_quote == '{convert_dict['curr_from']}' """)
        currency_rate_that_i_have_to_dollars = cursor.fetchone()[0]
        cursor.execute(f"""SELECT rates_in_quote
                                   FROM currency_quotes
                                   WHERE currency_in_quote == '{convert_dict['curr_to']}' """)
        currency_rate_that_i_need_to_dollars = cursor.fetchone()[0]

        # Calculation
        result = (currency_rate_that_i_need_to_dollars * float(convert_dict['amount_from']) /
                  currency_rate_that_i_have_to_dollars)
        convert_dict['result'] = round(result, 2)

        # return the result
        bot.reply_to(message,
                     f'{convert_dict["amount_from"]} {convert_dict["curr_from"]} = '
                     f'{convert_dict["result"]} {convert_dict["curr_to"]}',
                     parse_mode='html')
        bot.send_message(message.chat.id, 'спробуємо знову? \n /start')
    else:
        bot.reply_to(message, 'Щось не спрацювало, введіть дані знову, будь ласка \n /convert')


# send history
@bot.message_handler(commands=['history'])
def welcome_history(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    itembtn1 = types.KeyboardButton('Так')
    itembtn2 = types.KeyboardButton('Ні')
    markup.add(itembtn1, itembtn2)
    msg = bot.send_message(message.chat.id,
                           'Малюю графік зміни курсу вибраної валюти відносно USD за минулий тиждень \n'
                           'Продовжимо?', reply_markup=markup)
    bot.register_next_step_handler(msg, confirmation_history_and_curr_choice)


def confirmation_history_and_curr_choice(message):
    confirmation = message.text
    if confirmation == 'Так':
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        itembtn1 = types.KeyboardButton('EUR')
        itembtn2 = types.KeyboardButton('USD')
        itembtn3 = types.KeyboardButton('UAH')
        itembtn4 = types.KeyboardButton('AUD')
        itembtn5 = types.KeyboardButton('CAD')
        itembtn6 = types.KeyboardButton('RUB')
        markup.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5, itembtn6)
        msg = bot.send_message(message.chat.id,
                               'Виберіть валюту із запропонованих варіантів, '
                               'або введіть в форматі: UAH, наприклад.'
                               , reply_markup=markup)
        bot.register_next_step_handler(msg, make_rate_list)
    else:
        bot.reply_to(message,
                     'Поверніться в головне меню \n')
        bot.send_message(message.chat.id, '/start')


def make_rate_list(message):
    selected_currency_to_history = message.text
    bot.send_chat_action(message.chat.id, 'upload_photo')
    params_for_selected_currency = {f'access_key': config.TOKEN_CURRENCY,
                                    'symbols': selected_currency_to_history}
    params_for_usd = {f'access_key': config.TOKEN_CURRENCY,
                      'symbols': 'USD'}
    rates_list_history = []
    date_list_history = []
    try:
        for data in range(7):
            request_date = date.today() - timedelta(days=(7 - data))
            # get multiplier
            m = requests.get(f'http://data.fixer.io/api/{request_date}', params=params_for_usd)
            multiplier = 1 / m.json()['rates']['USD']
            # get selected_currency requests
            r = requests.get(f'http://data.fixer.io/api/{request_date}', params=params_for_selected_currency)
            rates_list_history.append(round(r.json()['rates'][selected_currency_to_history] * multiplier, 4))
            date_list_history.append('{:%m-%d}'.format(datetime.strptime(str(request_date), '%Y-%m-%d')))
    except:
        bot.send_message(message.chat.id, 'Дані курсу валют для обраної валюти наразі недоступні, спробуйте пізніше.')
        bot.send_message(message.chat.id, '/start')

    plot_dict['rates_list_history'] = rates_list_history
    plot_dict['date_list_history'] = date_list_history

    def save_file_png_plot_history(name='history_current', fmt='png'):
        pwd = os.getcwd()
        os.chdir(pwd)
        i_path = f'history_plots_{fmt}'
        if not os.path.exists(i_path):
            os.mkdir(i_path)
        os.chdir(i_path)
        plt.savefig(f'{name}.{fmt}')
        os.chdir(pwd)
        # plt.close()

    # make plot
    matplotlib.use('webagg')  # python -m pip install tornado
    plt.figure()
    plt.title(f'USD / {selected_currency_to_history}')
    plt.plot(plot_dict['date_list_history'], plot_dict['rates_list_history'], label='line', color='red')

    save_file_png_plot_history()

    # send plot to telegram
    picture = open('history_plots_png/history_current.png', 'rb')
    bot.send_photo(message.chat.id, picture)
    bot.send_message(message.chat.id, 'Поверніться в головне меню \n')
    bot.send_message(message.chat.id, '/start')


# chek if start answer is correct
@bot.message_handler(content_types=['text'])
def is_welcome(message):
    welcome = message.text
    if welcome not in ['/list', '/convert', '/history']:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        itembtn1 = types.KeyboardButton('/list')
        itembtn2 = types.KeyboardButton('/convert')
        itembtn3 = types.KeyboardButton('/history')
        markup.add(itembtn1, itembtn2, itembtn3)
        msg = bot.reply_to(message,
                           'Виберіть з доступних опцій, будь ласка \n'
                           , reply_markup=markup)


if __name__ == '__main__':
    response_dict = {'request_time': None, 'quote_numbers': None, 'currency_list': None, 'rates_list': None}
    convert_dict = {'curr_from': None, 'amount_from': None, 'curr_to': None, 'result': None}
    plot_dict = {'rates_list_history': None, 'date_list_history': None}
    make_request_and_save_response_to_db()
    get_response_data_from_db()
    bot.polling(none_stop=True)
